// The_Thirteenth.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Helpers.h"
#include <string>
#include <iomanip>

using namespace std;

class Animal
{
private:
    string Word = "Hi,dude!"; 
public:
    Animal() {}
    virtual void Voice()
    {
        cout << Word << endl;
   }
  
};


class Elephant : public Animal
{
private:
    string Word = "phruu, phruu, get out of my grass!";
public:
        Elephant() {}
        void Voice() override
        {
            cout << Word << "phruu!" << endl;
        }
   
};


class Alligator : public Animal
{
private:
    string Word = "come to me, motherfucker!!";
public:
    Alligator() {}
    void Voice() override
    {
        cout << Word << "chuvk, chuvk..." << endl;
    }
   
};


class Snake : public Animal
{
private:
    string Word = "You know nothing, Mowgli!";
public:
    Snake() {}
    void Voice() override
    {
        cout << Word <<  "pssssss!" << endl  ;
    }
    
};


int main()
{
    Alligator*  myAlligator = new Alligator();
    Snake*   mySnake = new Snake();
    Elephant*  myElephant = new Elephant();
    Animal* Animals[3];
    Animals[0] = myAlligator; 
    Animals[1] = mySnake; 
    Animals[2] = myElephant;
    for (int i = 0; i < 3; i++)
    {
        Animals[i]->Voice();
    }

    return 0;
}



// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
